#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h> // librairie du type bool�en
#include<assert.h> // librairie d'assertions
#include "lst_elm.h"
#include "lst.h"
#include "outils.h"


void listehomoreelle () {
	struct lst_t * L = new_lst ();
 	double * v;

	do {
		double u;

		printf ( "Entrez un r�el (0 pour s'arr�ter): " );
		scanf ("%lf", &u );
		//printf("1\n");
	if ( u == 0 ) break;
	//printf("2\n");
 	v = (double *) calloc (1, sizeof(double));
	//printf("3\n");
 	*v = u;
	//printf("4\n");
 	insert_ordered (L, v, &reelcmp);
	//printf("5\n");
 	}while(true);
 	view_lst(L, &printDouble);
 	del_lst (&L, &rmDouble);
}

void listehomoentiere () {
	struct lst_t * L = new_lst ();
 	int * v;

 	do {
 	int u;

 	printf ( "Entrez un entier (0 pour s'arr�ter): " );
 	scanf ("%d", &u );
 	if ( u == 0 ) break;

 	v = (int *) calloc ( 1, sizeof(int) );
 	*v = u;
 	insert_ordered (L,v,&intcmp);
 	}while ( true );

 	view_lst(L, &printInteger);
 	del_lst (&L, &rmInteger);
}

/***
	* ALGORITHME (FONCTION PRINCIPALE)
	***/
int main() {
	//listehomoentiere();
	//listehomoreelle();
	//return EXIT_SUCCESS;
 listehomoreelle ();
 listehomoentiere ();
	int cmpt , a = 8, b = 4, * ptra = &a, * ptrb = &b;
	double x = 5.4, y = 3.14, * ptrx = &x, * ptry = &y;
	struct lst_t * L = (struct lst_t *)calloc(1,sizeof(struct lst_t));
	struct lst_elm_t * E = (struct lst_elm_t *)calloc(1,sizeof(struct lst_elm_t));

	cons(L, ptra); // un entier
	cons(L, ptrb); // un entier
	cons(L, ptrx); // un r�el
	cons(L, ptry); // un r�el

	/* La liste vaut [3.14 ; 5.4 ; 4 ; 8 ] */
	for(cmpt = 0, E = L->head;
		cmpt < getNumelm(L);
		cmpt += 1, E = getSuc(E)) {
		if(cmpt < 2) {
			double * d = (double *) getDatum(E);
			printf( "La valeur est r�elle et vaut : %lf\n", *d );
		} else {
			int * d = (int *) getDatum(E);
			printf( "La valeur est enti�re et vaut : %d\n", *d );
		}
	}
	return EXIT_SUCCESS;
}

