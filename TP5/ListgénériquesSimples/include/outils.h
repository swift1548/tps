#ifndef _OUTILS_
#define _OUTILS_
#include <stdbool.h>
void printInteger ( int * i );
void rmInteger ( int * i );
bool intcmp ( int * i, int * j );
void printDouble ( double * d );
void rmDouble ( double * d );
bool reelcmp ( double * u, double * v );
void freeInteger(int *i);
#endif // _OUTILS_
