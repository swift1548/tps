#include <stdlib.h> // librairie standard
#include <stdio.h> // librairie input/output
#include <stdbool.h> // librairie du type booléen
#include <assert.h> // librairie d'assertions
#include "lst_elm_t.h"
struct lst_t {
	struct lst_elm_t* head;
	struct lst_elm_t* tail;
	int numelm;
};
struct lst_t* new_lst() {
	/* @note : calloc fonctionne de manière identique à malloc
		et de surcroît met à NULL(0) tous les octets alloués */
	struct lst_t* L = (struct lst_t*)calloc(1, sizeof(struct lst_t));
	assert(L);
	return L;
}
bool empty_lst(const struct lst_t*L) {
	assert(L);
	return L->numelm == 0;
}
void del_lst(struct lst_t** ptrL) {
	/* @ToDo */
	assert(ptrL && *ptrL);
	for (struct lst_elm_t* E = (*ptrL)->head; E; ) {
		struct lst_elm_t* F = E;
		E = E->suc;
		del_elm_t(&F);
	}
	free(*ptrL);
	*ptrL = NULL;
}

void cons(struct lst_t* L, int v) {
	/* @ToDo */
	assert(L);
	struct lst_elm_t* E = new_lst_elm(v);
	assert(E);
	E->suc = L->head;
	L->head = E;
	if (L->numelm == 0) L->tail = E;
	L->numelm += 1;

}
void print_lst(struct lst_t* L) {
	printf("[ ");
	for (struct lst_elm_t* E = L->head; E; E = E->suc) {
		printf("%d ", E->x);
	}
	printf("]\n\n");
}
void insert_after(struct lst_t* L, const int value, struct lst_elm_t** ptrl)
{
	if (*ptrl == NULL)
	{
		(*ptrl)->x = value;
		(*ptrl)->suc = NULL;
		L->head = (*ptrl);
		L->numelm++;
	}
	else
	{
		struct lst_elm_t* i;
		i->x = value;
		i->suc = (*ptrl)->suc;
		(*ptrl)->suc = i;
	}
}
void insert_ordered(struct lst_t* L, const int value)
{
	if (empty_lst(L))
	{
		L->head->x = value;
		L->head->suc = NULL;
		L->numelm++;
	}
	else
	{
		int a = L->head->x;
		int b = L->tail->x;
		if (value < a)
		{
			insert_after(L, value, L->head);
		}
		if (value > b)
		{
			insert_after(L, value, L->tail);
		}
		
		while (value > L->head->x)
		{
			L->head = L ->head->suc;
		}
		insert_after(L, value, L->head);
		L->numelm++;

	}
}
