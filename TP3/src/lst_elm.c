#include <stdlib.h> // librairie standard
#include <stdio.h> // librairie input/output
#include <stdbool.h> // librairie du type booléen
#include <assert.h> // librairie d'assertions
struct lst_elm_t {
	int x;
	struct lst_elm_t* suc;
};
struct lst_elm_t* new_lst_elm(int value)
{
	struct lst_elm_t* L = (struct lst_elm_t*)calloc(1, sizeof(struct lst_elm_t));
	L->x = value;
	return L;
}
/* @brief Supprimer un élément de liste et mettre son pointeur à NULL */

int getX(struct lst_elm_t* E)
{
	return E->x;
}
/* @brief Renvoyer le pointeur sur le successeur de l'élément */
struct lst_elm_t* getSuc(struct lst_elm_t* E)
{
	return E->suc;
}
/* @brief Modifier la valeur entière de l'élément */
void setX(struct lst_elm_t* E, int v)
{
	E->x = v;
}
/* @brief Modifier le pointeur sur le successeur de l'élément */
void setSuc(struct lst_elm_t* E, struct lst_elm_t* S)
{
	E->suc = S;
}
void del_elm_t(struct lst_elm_t** ptrE)
{
	assert(ptrE && *ptrE);
	free(*ptrE);
	*ptrE = NULL;
}
