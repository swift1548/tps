#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "outils.h"
#include "form.h"
#include "d.h"

struct form_t * new_form(){
  struct form_t * F = calloc(1,sizeof(struct form_t));
  assert(F);
  return F;
}
void del_form(struct form_t ** ptrF) {
  assert(ptrF && *ptrF);
  free(*ptrF);
  *ptrF = NULL;
}

struct form_t * read_form(FILE * fd, enum mode_t mode) {
  assert(fd);

  struct form_t * F = calloc(1, sizeof(struct form_t));
  assert(F);

  if(mode == TEXT) {
    fscanf(fd, " %s", F->product);
    F->product[len_max]='\0';
    fscanf(fd, " %d", &(F->stock));
    fscanf(fd, " %lf", &(F->pc));
  } else { // BINARY
    fread(F, sizeof(struct form_t), 1, fd);
  }
  if(F && strlen(F->product) == 0) {
   
    printf("NULL\n");
  
    return NULL;
  }
 
  view_form(F);

  return F;
}

void write_form(struct form_t * F, enum mode_t mode, FILE * fd) {
  assert(fd);

  if(mode == TEXT) {
      printf("view_form inside write_form\n");
      view_form(F);
    fprintf(fd, "%s\n", get_product(F));
    fprintf(fd, "%d\n", get_stock(F));
    fprintf(fd, "%lf\n", get_price(F));
  } else {
    fwrite(F, sizeof(struct form_t), 1, fd);
  }
}

char * get_product(struct form_t * F) {
  assert(F != NULL);
  return F->product;
}

int get_stock(struct form_t * F) {
  assert(F != NULL);
  return F->stock;
}

double get_price(struct form_t * F) {
  assert(F != NULL);
  return F->pbt;
}

void view_form(struct form_t * F) {
  assert(F != NULL);
    printf("Nom du produit : %s\n",F->product);
    printf("Nombre en stock : %d\n",F->stock);
    printf("Prix: %lf\n",F->pbt);
  );
}

bool gt_form(struct form_t * F1, struct form_t * F2) {
  assert(F1 != NULL && F2 != NULL);
  return gt_string(F1->product, F2->product);
}

bool lt_form(struct form_t * F1, struct form_t * F2) {
  assert(F1 != NULL && F2 != NULL);
  return lt_string(F1->product, F2->product);
}
